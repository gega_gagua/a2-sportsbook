import { SingularPage } from './app.po';

describe('singular App', function() {
  let page: SingularPage;

  beforeEach(() => {
    page = new SingularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

// import a2 default modules
import { BrowserModule }   from '@angular/platform-browser';
import { NgModule }        from '@angular/core';
import { FormsModule }     from '@angular/forms';
import { HttpModule }      from '@angular/http';
import { SortName }        from './pipes/sort';
import { SortNumber }      from './pipes/sort-number';
import { SortNumberAsc }   from './pipes/sort-number-asc';
import { SortOdds }        from './pipes/sort-odds';

// import app
import { SportsBook }      from './components/sports-book/sports-book';
import { MatchesGrid }      from './components/matches-grid/matches-grid';

@NgModule({
  declarations: [
    SportsBook,
    MatchesGrid,
    SortName,
    SortNumber,
    SortNumberAsc,
    SortOdds
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [SportsBook]
})
export class AppModule { }

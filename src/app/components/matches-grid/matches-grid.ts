import { Component, Input } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
    selector:     'matches-list',
    templateUrl:  './matches-grid.html',
    styleUrls:    ['./matches-grid.scss']
})

export class MatchesGrid {
  
    layout  = [];
    matches = [];
    @Input('activeLeagueID') activeLeagueID;

    /*
    * get layout from tree.json
    */
    constructor(private http:Http) {
        
        this.http.get('../assets/data/layout.json')
        .subscribe( res => {
        
                //get data
                let layoutData = res.json().data;
                this.layout = layoutData; 

            } 
        );
        
    }


    /*
    * Active leage on change
    */
    ngOnChanges(changes: any) {

        let activeLeague = changes.activeLeagueID.currentValue;
        
        if ( typeof activeLeague == 'number' ) {
            
            this.getMatches( activeLeague );

        }

    }


    /*
    * get matches by ID
    */
    getMatches( ID ) {
        
        this.matches = [];
        this.http.get(`../assets/data/matches/league_${ID}.json`)
        .subscribe( result => this.matches = result.json().data,
                    err => console.log(err),
                    () => console.log( 'success' )
        );

    }


}

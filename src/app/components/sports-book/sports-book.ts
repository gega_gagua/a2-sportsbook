import { Component } from '@angular/core';
import { Http } from '@angular/http';


@Component({
  selector:     'app-root',
  templateUrl:  './sports-book.html',
  styleUrls:    ['./sports-book.scss']
})

export class SportsBook {

    activeSport;
    activeCountry;
    activeLeague;
    activeLeagueID;
    data  = [];


    /*
    * get data from tree.json
    */
    constructor(private http:Http) {
        
        this.http.get('../assets/data/tree.json')
        .subscribe( res => {
            
                //get data
                let tree = res.json().data;
                this.data = tree; 

            } 
        );

        
    }


    /*
    * Change Active menu
    */
    changeActiveIndex(i,list) {

        ( this[list] != i ) ? this[list] = i : this[list] = '';
    
    }

    /*
    * filter matched by league ID
    */
    getData( ID ) {
        
        this.activeLeagueID = ID;

    }
}

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: "sortNumber"
})

/*
* Sort by name
*/
export class SortNumber implements PipeTransform {
    transform(array:Array<any>): Array<any>{

        array.sort((a: any, b: any) => {
            
            return parseFloat(b.priority) - parseFloat(a.priority);

        });
        
        return array;
    }
}
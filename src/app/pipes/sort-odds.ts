import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: "sortOdds"
})

/*
* Sort by name
*/
export class SortOdds implements PipeTransform {

    transform(array:Array<any>, args:Array<any>): Array<any>{

        let result = [];
        let sort   = [];

        if ( typeof args == 'undefined' || args.length == 0 ) {

            return array;

        }else {

            if ( args.length > array.length ) {
                   
                args.forEach(function(sortKey){

                    sort.push( sortKey.id );

                    if ( args.length > array.length && array.indexOf( { 'name' : sortKey.id } ) < 0 ) {
                          
                        array.push({ 'name' : sortKey.id, 'value' : '-' });

                    }

                })

            }


            result = array.map(function(item) {
                var n = sort.indexOf(item.name);
                sort[n] = '';
                return [n, item]
            }).sort().map(function(j) { return j[1] })

        }
        
        return result;
    }
}
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: "sortNumberAsc"
})

/*
* Sort by name
*/
export class SortNumberAsc implements PipeTransform {
    transform(array:Array<any>): Array<any>{

        array.sort((a: any, b: any) => {
            
            return parseFloat(a.priority) - parseFloat(b.priority);

        });
        
        return array;
    }
}